package main

import (
	"bufio"
	"log"
	"os"
)

func getlines(readFile string, gList *[]string) {
	file, err := os.Open(readFile)
	defer file.Close()
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		*gList = append(*gList, scanner.Text())
	}
}
