package main

import (
	"net/http"
	"net/http/cookiejar"
)

func main() {

	userfile := "./passlists/testuser"
	passfile := "./passlists/testpass"
	jar, _ := cookiejar.New(nil)
	app := App{
		Client: &http.Client{Jar: jar},
	}
	getlines(userfile, &usernameList)
	getlines(passfile, &passwordList)
	for i := 0; i <= len(usernameList)-1; i++ {
		testCreds(&app, usernameList[i])
	}

}
